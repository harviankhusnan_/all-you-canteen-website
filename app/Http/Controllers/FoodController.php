<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Http\Requests\StoreFoodRequest;
use App\Http\Requests\UpdateFoodRequest;
use Illuminate\Http\Request;
use Illuminate\Session\Store;

class FoodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data['nav_foods'] = true;
        $data['foods_columns'] = ['No', 'Kode', 'Nama', 'Lokasi Kantin', 'Kuantitas', 'Harga'];
        $data['foods'] = Food::all();
        return view('dashboards.foods')->withData($data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'food_code' => 'required',
            'food_name' => 'required',
            'food_price' => 'required',
            'food_quantity' => 'required',
        ]);

        $new_food = new Food;
        $new_food->code = $request->food_code;
        $new_food->name = $request->food_name;
        $new_food->price = $request->food_price;
        $new_food->quantity = $request->food_quantity;
        $new_food->save();

        return redirect()->route('foods');
    }

    /**
     * Display the specified resource.
     */
    public function show(Food $food)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, $id)
    {
        $request->validate([
            'food_code' => 'required',
            'food_name' => 'required',
            'food_price' => 'required',
            'food_quantity' => 'required',
        ]);

        $food = Food::find($id);

        if ($food != null) {
            $food->code = $request->food_code;
            $food->name = $request->food_name;
            $food->price = $request->food_price;
            $food->quantity = $request->food_quantity;
            $food->save();

            return redirect()->route('foods');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete(int $id)
    {
        $food = Food::where('id', $id)->first();

        if ($food != null) {
            $food->delete();

            return redirect(route('foods'));
        }
    }
}
