FROM php:8.1.6-alpine
RUN apk update && apk add openssl zip unzip git libpng-dev nodejs npm
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-configure gd
RUN docker-php-ext-install gd mysqli pdo pdo_mysql
WORKDIR /app
COPY . /app

ENV COMPOSER_ALLOW_SUPERUSER=1

RUN composer install --no-scripts
RUN npm install
RUN npm run build

RUN cat .env.example > .env

CMD printf "DB_PASSWORD=$DB_PASSWORD\nDB_HOST=$DB_HOST\n" >> .env | php artisan migrate | php artisan passport:install | php artisan serve --host=0.0.0.0 --port=8080
EXPOSE 8080
