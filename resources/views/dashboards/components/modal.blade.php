<!-- Form Modal -->
<div class="modal fade" id="foods-form" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('store.foods') }}" method="post">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Makanan</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="input-kode-makanan" class="form-label">Kode Makanan</label>
                        <input type="text" name="food_code" id="input-kode-makanan" class="form-control">
                        <div class="form-text">
                            Kode makanan berupa 6 digit karakter, contoh: TD44044
                            * Dua karakter awal diawali oleh kode kantin
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="input-nama-makanan" class="form-label">Nama Makanan atau Minuman</label>
                        <input type="text" name="food_name" id="input-nama-makanan" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="input-harga" class="form-label">Harga</label>
                        <input type="number" name="food_price" id="input-harga" class="form-control">
                        <div class="form-text">
                            Harga dalam satuan Rupiah (Rp.)
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="input-quantity" class="form-label">Kuantitas</label>
                        <input type="number" name="food_quantity" id="input-quantity" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
