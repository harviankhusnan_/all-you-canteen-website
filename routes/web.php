<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/new-ui', function() {
   return view('layouts.new-app');
});


Route::prefix('dashboard')->group(function() {
    // Home
    Route::get('/home', [HomeController::class, 'index'])->name('home');

    // Foods
    Route::prefix('foods')->group(function() {
        Route::get('/', [FoodController::class, 'index'])->name('foods');
        Route::post('/', [FoodController::class, 'store'])->name('store.foods');
        Route::delete('/{id}', [FoodController::class, 'delete'])->name('delete.foods');
        Route::put('/{id}', [FoodController::class, 'edit'])->name('edit.foods');
    });

    // Canteens
    Route::get('/canteens', [HomeController::class, 'canteens'])->name('canteens');

    // Another Place
    Route::get('/another-places', [HomeController::class, 'anotherPlaces'])->name('anotherplaces');
})->middleware('auth');
